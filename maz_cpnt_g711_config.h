/*
 * maz_cpnt_g711_config.h
 *
 *  Created on: 2021年03月14日
 *      Author: wangbing
 *      Email : mz8023yt@163.com
 *      Gitee : https://gitee.com/mazcpnt/maz-g711
 */

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _MAZ_CPNT_G711_CONFIG_H_
#define _MAZ_CPNT_G711_CONFIG_H_

/* S16 PCM 数据是大端序还是小端序 */
#define MAZCPNT_G711_ENDIAN_LITTLE      (0)
#define MAZCPNT_G711_ENDIAN_BIG         (1)
#define MAZCPNT_G711_ENDIAN             MAZCPNT_G711_ENDIAN_BIG

#endif /* _MAZ_CPNT_G711_CONFIG_H_ */

#ifdef __cplusplus
}
#endif

