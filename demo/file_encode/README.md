# 使用 MAZ-G.711 编码 G.711

输入 PCM 文件，输出 G.711 编码文件。

## 编译

```
paul@vmware:~/study/maz-g711/demo/file_encode$ make
gcc -I./ -I../../  -c file_encode.c -o file_encode.o
gcc file_encode.o ../../maz_cpnt_g711.o -I./ -I../../  -o file_encode
paul@vmware:~/study/maz-g711/demo/file_encode$ 
```

 ## 编码 G.711A

```
paul@vmware:~/study/maz-g711/demo/file_encode$ ./file_encode alaw dayu_8k_1ch_s16le.pcm 1.alaw
iname    = dayu_8k_1ch_s16le.pcm
oname    = 1.alaw
isize    = 480000
osize    = 240000
G.711 ALAW encode finish!
paul@vmware:~/study/maz-g711/demo/file_encode$
```

## 编码 G.711U

```
paul@vmware:~/study/maz-g711/demo/file_encode$ ./file_encode ulaw dayu_8k_1ch_s16le.pcm 1.ulaw
iname    = dayu_8k_1ch_s16le.pcm
oname    = 1.ulaw
isize    = 480000
osize    = 240000
G.711 ULAW encode finish!
paul@vmware:~/study/maz-g711/demo/file_encode$ 
```

