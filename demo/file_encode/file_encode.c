#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "maz_cpnt_g711.h"

uint32_t get_file_size(char *file_name);

int main(int argc, char **argv)
{
    int ret = 0;
    int16_t ival = 0;
    uint8_t oval = 0;
    MAZCPNT_E_G711_TYPE type;

    char        iname[32];
    FILE       *ifile       = NULL;
    uint8_t    *ibuf        = NULL;
    uint32_t    isize       = 0;
    int         ircnt       = 0;

    char        oname[32];
    FILE       *ofile       = NULL;
    uint8_t    *obuf        = NULL;
    uint32_t    osize       = 0;
    int         owcnt       = 0;

    /* 打印函数使用方法 */
    if (argc != 4)
    {
        printf("usage: %s <type> <pcm file name> <g711 file name>\n", argv[0]);
        printf("eg   : %s alaw hello.pcm hello.g711a\n", argv[0]);
        printf("eg   : %s ulaw hello.pcm hello.g711u\n", argv[0]);
        printf("\n");
        return -1;
    }

    if(!strcmp(argv[1], "alaw"))
    {
        type = MAZCPNT_E_G711_TYPE_ALAW;
    }
    else if(!strcmp(argv[1], "ulaw"))
    {
        type = MAZCPNT_E_G711_TYPE_ULAW;
    }
    else
    {
        printf("please enter g.711 encode type:\n");
        printf("  eg1: alaw\n");
        printf("  eg2: ulaw\n");
        return -1;
    }

    /* 解析输入参数得到文件名 */
    strcpy(iname, argv[2]);
    strcpy(oname, argv[3]);
    printf("iname    = %s\n", iname);
    printf("oname    = %s\n", oname);

    /* 获取输入文件大小, 计算输出文件大小 */
    isize = get_file_size(iname);
    osize = isize >> 1;
    printf("isize    = %d\n", isize);
    printf("osize    = %d\n", osize);

    /* 申请数据内存 */
    ibuf = (uint8_t *)malloc(isize);
    obuf = (uint8_t *)malloc(osize);
    memset(ibuf, 0, isize);
    memset(obuf, 0, osize);

    /* 读取输出数据 */
    ifile = fopen(iname, "r");
    MAZASSERT_RETVAL(NULL == ifile, -1, "err: can not open %s file", iname);

    ircnt = fread(ibuf, 1, isize, ifile);
    fclose(ifile);
    MAZASSERT_RETVAL(ircnt != isize, ret, "err: fread, rcnt = %d", ircnt);

#if 0
    /* 测试数据拼接 */
    printf("1 0x%02x\n", ibuf[0]);
    printf("2 0x%02x\n", ibuf[1]);
    printf("3 0x%04hx\n", ibuf[1] << 8 + ibuf[0]);
    printf("4 0x%04hx\n", ibuf[1] << 8 | ibuf[0]);
#endif

    /* 音频编码 */
    MAZ_CPNT_g711_encode(type, ibuf, obuf, isize);

    /* 输出到文件 */
    ofile = fopen(oname, "wb");
    MAZASSERT_RETVAL(NULL == ofile, -1, "err: can not open %s file", oname);

    owcnt = fwrite(obuf, 1, osize, ofile);
    fclose(ofile);
    MAZASSERT_RETVAL(owcnt != osize, ret, "err: fwrite, wcnt = %d", owcnt);

    free(ibuf);
    free(obuf);

    printf("G.711 %s encode finish!\n", MAZCPNT_E_G711_TYPE_STR(type));

    return 0;
}

uint32_t get_file_size(char *file_name)
{
    struct stat buf;
    if(stat(file_name, &buf) < 0)
    {
        return 0;
    }
    return (uint32_t)buf.st_size;
}

