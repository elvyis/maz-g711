#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "maz_cpnt_g711.h"

int main(int argc, char **argv)
{
    int ret = 0;
    uint8_t ival = 0;
    int16_t oval = 0;
    MAZCPNT_E_G711_TYPE type;

    if(argc !=  3)
    {
        printf("usage: %s <type> <8bit G.711>\n", argv[0]);
        printf("eg   : %s alaw 0xE6\n", argv[0]);
        printf("eg   : %s ulaw 0xE6\n", argv[0]);
        printf("\n");
        return -1;
    }

    if(!strcmp(argv[1], "alaw"))
    {
        type = MAZCPNT_E_G711_TYPE_ALAW;
    }
    else if(!strcmp(argv[1], "ulaw"))
    {
        type = MAZCPNT_E_G711_TYPE_ULAW;
    }
    else
    {
        printf("please enter g.711 encode type:\n");
        printf("  eg1: alaw\n");
        printf("  eg2: ulaw\n");
        return -1;
    }

    if(strlen(argv[2]) != 4)
    {
        printf("please enter full g.711 value\n");
        printf("  eg1: 0xFF\n");
        printf("  eg2: 0x34\n");
        printf("The following format is incorrect\n");
        printf("  err fmt1: 0x4d2\n");
        printf("  err fmt2: fe\n");
        printf("  err fmt3: 0xxFF\n\n");
        return -1;
    }

    ival = (uint8_t)strtol(argv[2], NULL, 0);
    ret = MAZ_CPNT_g711_decode_one(type, ival, (uint8_t *)&oval);
    MAZASSERT_RETVAL(ret, -1, "err: MAZ_CPNT_g711_encode_one");

    printf("G.711 %s decode: 0x%02X --> 0x%04hX\n", MAZCPNT_E_G711_TYPE_STR(type), ival, oval);

    return 0;
}

