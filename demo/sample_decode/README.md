# 使用 MAZ-G.711 解码一个采样

输入 G.711 编码单个采样点，输出  PCM 值。

## 编译

```
paul@vmware:~/study/maz-g711/demo/sample_decode$ make
gcc -I./ -I../../  -c sample_decode.c -o sample_decode.o
gcc sample_decode.o ../../maz_cpnt_g711.o -I./ -I../../  -o sample_decode
paul@vmware:~/study/maz-g711/demo/sample_decode$ 
```

 ## 解码 G.711A

```
paul@vmware:~/study/maz-g711/demo/sample_decode$ ./sample_decode alaw 0xE6
G.711 ALAW decode: 0xE6 --> 0x04E0
paul@vmware:~/study/maz-g711/demo/sample_decode$
```

## 解码 G.711U

```
paul@vmware:~/study/maz-g711/demo/sample_decode$ ./sample_decode ulaw 0xCC
G.711 ULAW decode: 0xCC --> 0x04E0
paul@vmware:~/study/maz-g711/demo/sample_decode$ 
```

