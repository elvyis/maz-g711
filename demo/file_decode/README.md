# 使用 MAZ-G.711 解码 G.711

输入 G.711 文件，输出 PCM 文件。

## 编译

```
paul@vmware:~/study/maz-g711/demo/file_decode$ make
gcc -I./ -I../../  -c file_decode.c -o file_decode.o
gcc file_decode.o ../../maz_cpnt_g711.o -I./ -I../../  -o file_decode
paul@vmware:~/study/maz-g711/demo/file_decode$ 
```

## 解码 G.711A

```
paul@vmware:~/study/maz-g711/demo/file_decode$ ./file_decode alaw dayu_8k_1ch_s16le.alaw 1.pcm
iname    = dayu_8k_1ch_s16le.alaw
oname    = 1.pcm
isize    = 240000
osize    = 480000
G.711 ALAW decode finish!
paul@vmware:~/study/maz-g711/demo/file_decode$ 
```

## 解码 G.711U

```
paul@vmware:~/study/maz-g711/demo/file_decode$ ./file_decode ulaw dayu_8k_1ch_s16le.ulaw 2.pcm
iname    = dayu_8k_1ch_s16le.ulaw
oname    = 2.pcm
isize    = 240000
osize    = 480000
G.711 ULAW decode finish!
paul@vmware:~/study/maz-g711/demo/file_decode$ 
```

