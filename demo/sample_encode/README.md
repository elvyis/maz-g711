# 使用 MAZ-G.711 编码一个采样

输入 PCM 单个采样点，输出 G.711 编码值。

## 编译

```
paul@vmware:~/study/maz-g711/demo/sample_encode$ make
gcc -I./ -I../../  -c sample_encode.c -o sample_encode.o
gcc sample_encode.o ../../maz_cpnt_g711.o -I./ -I../../  -o sample_encode
paul@vmware:~/study/maz-g711/demo/sample_encode$ 
```

 ## 编码 G.711A

```
paul@vmware:~/study/maz-g711/demo/sample_encode$ ./sample_encode alaw 0x04d2
G.711 ALAW encode: 0x04D2 --> 0xE6
paul@vmware:~/study/maz-g711/demo/sample_encode$ 
```

## 编码 G.711U

```
paul@vmware:~/study/maz-g711/demo/sample_encode$ ./sample_encode ulaw 0x04d2
G.711 ULAW encode: 0x04D2 --> 0xCC
paul@vmware:~/study/maz-g711/demo/sample_encode$ 
```

