/*
 * maz_cpnt_g711.h
 *
 *  Created on: 2021年03月13日
 *      Author: wangbing
 *      Email : mz8023yt@163.com
 *      Gitee : https://gitee.com/mazcpnt/maz-g711
 */

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _MAZ_CPNT_G711_H_
#define _MAZ_CPNT_G711_H_

#include "stdio.h"

#define MAZRET_OK           0
#define MAZRET_NG           -1
#define MAZRET_EINVAL       -2      /* Invalid argument */

/* 数据结构 */
#define int8_t              char
#define int16_t             short
#define int32_t             int

#define uint8_t             unsigned char
#define uint16_t            unsigned short
#define uint32_t            unsigned int

#define MAZASSERT_RETVAL(condition, ret, fmt, msg...)                   \
    if (condition)                                                      \
    {                                                                   \
        printf(fmt "\r\n", ##msg);                                      \
        return ret;                                                     \
    }

#define MAZASSERT_RETVAL_NOMSG(condition, ret)                          \
    if (condition)                                                      \
    {                                                                   \
        return ret;                                                     \
    }

/**
 * @brief 组件版本号
 */
#define MAZCPNT_G711_MAIN_VER       1
#define MAZCPNT_G711_SUB_VER        1
#define MAZCPNT_G711_REV_VER        0

/**
 * @brief G.711 编码类型
 */
typedef enum _MAZCPNT_E_G711_TYPE_
{
    MAZCPNT_E_G711_TYPE_ALAW = 0,
    MAZCPNT_E_G711_TYPE_ULAW,
} MAZCPNT_E_G711_TYPE;

#define MAZCPNT_E_G711_TYPE_STR(en)     \
    (en == MAZCPNT_E_G711_TYPE_ALAW) ? "ALAW" : \
    (en == MAZCPNT_E_G711_TYPE_ULAW) ? "ULAW" : "UNKNOW"

/**
 * @brief G.711A编码
 * @param ibuf 输入的PCM数据
 * @param obuf 输出的G.711A数据
 * @param length 输入的PCM数据长度
 */
int MAZ_CPNT_g711a_encode(uint8_t *ibuf, uint8_t *obuf, uint32_t length);

/**
 * @brief G.711U编码
 * @param ibuf 输入的PCM数据
 * @param obuf 输出的G.711U数据
 * @param length 输入的PCM数据长度
 */
int MAZ_CPNT_g711u_encode(uint8_t *ibuf, uint8_t *obuf, uint32_t length);

/**
 * @brief G.711编码
 * @param type G.711编码类型
 * @param ibuf 输入的PCM数据
 * @param obuf 输出的G.711数据
 * @param length 输入的PCM数据长度
 */
int MAZ_CPNT_g711_encode(MAZCPNT_E_G711_TYPE type, uint8_t *ibuf, uint8_t *obuf, uint32_t length);

/**
 * @brief G.711A编码单个采样点
 * @param ibuf 输入的PCM数据
 * @param obuf 输出的G.711A数据
 */
int MAZ_CPNT_g711a_encode_one(int16_t ibuf, uint8_t *obuf);

/**
 * @brief G.711U编码单个采样点
 * @param ibuf 输入的PCM数据
 * @param obuf 输出的G.711U数据
 */
int MAZ_CPNT_g711u_encode_one(int16_t ibuf, uint8_t *obuf);

/**
 * @brief G.711编码单个采样点
 * @param type G.711编码类型
 * @param ibuf 输入的PCM数据
 * @param obuf 输出的G.711数据
 */
int MAZ_CPNT_g711_encode_one(MAZCPNT_E_G711_TYPE type, int16_t ibuf, uint8_t *obuf);

/**
 * @brief G.711A解码
 * @param ibuf 输入的G.711A数据
 * @param obuf 输出的PCM数据
 * @param length 输入的G.711A数据长度
 */
int MAZ_CPNT_g711a_decode(uint8_t *ibuf, uint8_t *obuf, uint32_t length);

/**
 * @brief G.711U解码
 * @param ibuf 输入的G.711U数据
 * @param obuf 输出的PCM数据
 * @param length 输入的G.711U数据长度
 */
int MAZ_CPNT_g711u_decode(uint8_t *ibuf, uint8_t *obuf, uint32_t length);

/**
 * @brief G.711解码
 * @param type G.711编码类型
 * @param ibuf 输入的G.711数据
 * @param obuf 输出的PCM数据
 * @param length 输入的G.711数据长度
 */
int MAZ_CPNT_g711_decode(MAZCPNT_E_G711_TYPE type, uint8_t *ibuf, uint8_t *obuf, uint32_t length);

/**
 * @brief G.711A解码单个采样点
 * @param ibuf 输入的G.711A数据
 * @param obuf 输出的PCM数据
 */
int MAZ_CPNT_g711a_decode_one(uint8_t ibuf, uint8_t *obuf);

/**
 * @brief G.711U解码单个采样点
 * @param ibuf 输入的G.711U数据
 * @param obuf 输出的PCM数据
 */
int MAZ_CPNT_g711u_decode_one(uint8_t ibuf, uint8_t *obuf);

/**
 * @brief G.711解码单个采样点
 * @param type G.711编码类型
 * @param ibuf 输入的G.711数据
 * @param obuf 输出的PCM数据
 */
int MAZ_CPNT_g711_decode_one(MAZCPNT_E_G711_TYPE type, uint8_t ibuf, uint8_t *obuf);


#endif /* _MAZ_CPNT_G711_H_ */

#ifdef __cplusplus
}
#endif

